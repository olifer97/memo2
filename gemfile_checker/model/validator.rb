class Validator
  def process(gemfile_content)
    return 'Error: Gemfile vacio' if gemfile_content.empty?
    return 'Error: Gemfile sin source' unless gemfile_content.include? 'source'
    return 'Error: Gemfile sin version de ruby' unless gemfile_content.match?(/ruby '[0-9].[0-9].[0-9]'/)

    gems_last = ''

    gemfile_content.each_line do |line|
      next if line.strip.empty?

      return false unless line.start_with?('ruby', 'source', 'gem')

      next unless line.start_with?('gem')

      gem_name = line[/'(.*?)'/m, 1]
      return 'Error: Las gemas no estan ordenadas alfabeticamente' unless gems_last < gem_name

      gems_last = gem_name
    end

    'Gemfile correcto'
  end
end
