source 'https://rubygems.org'

gem 'byebug'
gem 'guard'
gem 'guard-rspec'
gem 'rake'
gem 'rb-readline'
gem 'rspec'
gem 'rubocop'
gem 'rubocop-rspec'
gem 'simplecov', require: false, group: :test
